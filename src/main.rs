use clap::Parser;
use log::*;
use std::thread::sleep;
use std::time::Duration;
use tokio_nsq::*;
use rand::prelude::*;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Broker addr
    #[clap(short, long, value_parser, default_value = "127.0.0.1:4150")]
    addr: String,

    /// Humidity topic name
    #[clap(long, value_parser, default_value = "humidity")]
    humidity_topic: String,

    /// Humidity topic name
    #[clap( long, value_parser, default_value = "angle")]
    angle_topic: String,
}

async fn publish(addr: &std::string::String, topic_h: &std::string::String, topic_a: &std::string::String) {
    let mut rng = rand::thread_rng();
    let h_topic = NSQTopic::new(topic_h).unwrap();
    let a_topic = NSQTopic::new(topic_a).unwrap();

    let mut producer = NSQProducerConfig::new(addr).build();
    let x = producer.consume().await.unwrap();

    info!("{:?}", x);
    loop {
        let humidity: u8 = rng.gen_range(0..100);
        let angle: u16 = rng.gen_range(0..360);

        producer.publish(&h_topic, humidity.to_string().as_bytes().to_vec()).await;
        info!("humidity {:?}", producer.consume().await.unwrap());

        producer.publish(&a_topic, angle.to_string().as_bytes().to_vec()).await;
        info!("angle {:?}", producer.consume().await.unwrap());
        sleep(Duration::new(0, 3));
    }
}

fn main() {
    env_logger::init();

    let args = Args::parse();
    info!("starting");
    debug!("start");
    let rt = tokio::runtime::Runtime::new().unwrap();
    let future = publish(&args.addr, &args.humidity_topic, &args.angle_topic);
    rt.block_on(future);
    // println!("Hello, world!");
}
