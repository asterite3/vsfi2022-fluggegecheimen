{ pkgs ? import <nixpkgs> {}}:
let
  unstable = import <unstable> { config.allowUnfree = true; };
in
pkgs.mkShell {
  buildInputs = with pkgs; [
    # unstable.rustc
    unstable.rustup
    # unstable.cargo
    rustfmt
    rust-analyzer
    clippy
  ];

  RUST_BACKTRACE = 1;
}
